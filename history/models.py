# -*- coding: utf-8 -*-

from django.db import models

from tinymce import models as tinymce_models

from shelter.models import Shelter


class History(models.Model):
    order_number = models.FloatField(verbose_name="Порядковый номер", blank=True, null=True)
    name = models.CharField(max_length=1024, verbose_name="Название")
    shelter = models.ForeignKey(Shelter, verbose_name="Приют", on_delete=models.CASCADE, default=1)
    date = models.DateTimeField(verbose_name='Дата', blank=True, null=True)
    text = tinymce_models.HTMLField(verbose_name="Текст", blank=True, null=True)
    image = models.ImageField(upload_to='uploads/images', verbose_name="Картинка", blank=True, null=True)
    isHidden = models.BooleanField(verbose_name="Скрыть", blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["order_number"]
        verbose_name_plural = u"Истории успеха"
