# -*- coding: utf-8 -*-
from django.contrib import admin

from history.models import History


class HistoryAdmin(admin.ModelAdmin):
    list_display = ('name',)


admin.site.register(History,HistoryAdmin)
