(function($) {

	'use strict';

	// bootstrap dropdown hover

  // loader
  var loader = function() {
    setTimeout(function() { 
      if($('#loader').length > 0) {
        $('#loader').removeClass('show');
      }
    }, 1);
  };
  loader();

	
	$('nav .dropdown').hover(function(){
		var $this = $(this);
		$this.addClass('show');
		$this.find('> a').attr('aria-expanded', true);
		$this.find('.dropdown-menu').addClass('show');
	}, function(){
		var $this = $(this);
			$this.removeClass('show');
			$this.find('> a').attr('aria-expanded', false);
			$this.find('.dropdown-menu').removeClass('show');
	});


	$('#dropdown04').on('show.bs.dropdown', function () {
	  console.log('show');
	});

	// home slider
	$('.home-slider').owlCarousel({
    loop:true,
    autoplay: true,
    margin:10,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    nav:true,
    autoplayHoverPause: true,
    items: 1,
    navText : ["<span class='ion-chevron-left'></span>","<span class='ion-chevron-right'></span>"],
    responsive:{
      0:{
        items:1,
        nav:false
      },
      600:{
        items:1,
        nav:false
      },
      1000:{
        items:1,
        nav:true
      }
    }
	});

	// owl carousel
	var majorCarousel = $('.js-carousel-1');
	majorCarousel.owlCarousel({
    loop:true,
    autoplay: true,
    stagePadding: 7,
    margin: 20,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    nav: true,
    autoplayHoverPause: true,
    items: 3,
    navText : ["<span class='ion-chevron-left'></span>","<span class='ion-chevron-right'></span>"],
    responsive:{
      0:{
        items:1,
        nav:false
      },
      600:{
        items:2,
        nav:false
      },
      1000:{
        items:3,
        nav:true,
        loop:false
      }
  	}
	});

	// owl carousel
	var major2Carousel = $('.js-carousel-2');
	major2Carousel.owlCarousel({
    loop:true,
    autoplay: true,
    stagePadding: 7,
    margin: 20,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    nav: true,
    autoplayHoverPause: true,
    items: 4,
    navText : ["<span class='ion-chevron-left'></span>","<span class='ion-chevron-right'></span>"],
    responsive:{
      0:{
        items:1,
        nav:false
      },
      600:{
        items:3,
        nav:false
      },
      1000:{
        items:4,
        nav:true,
        loop:false
      }
  	}
	});


	var contentWayPoint = function() {
		var i = 0;
		$('.element-animate').waypoint( function( direction ) {

			if( direction === 'down' && !$(this.element).hasClass('element-animated') ) {
				
				i++;

				$(this.element).addClass('item-animate');
				setTimeout(function(){

					$('body .element-animate.item-animate').each(function(k){
						var el = $(this);
						setTimeout( function () {
							var effect = el.data('animate-effect');
							if ( effect === 'fadeIn') {
								el.addClass('fadeIn element-animated');
							} else if ( effect === 'fadeInLeft') {
								el.addClass('fadeInLeft element-animated');
							} else if ( effect === 'fadeInRight') {
								el.addClass('fadeInRight element-animated');
							} else {
								el.addClass('fadeInUp element-animated');
							}
							el.removeClass('item-animate');
						},  k * 100);
					});
					
				}, 100);
				
			}

		} , { offset: '95%' } );
	};

	contentWayPoint();

    $(".timeout").hide();
    setTimeout($(".timeout").fadeIn(500), 2000);

    let width = window.outerWidth;
    let phoneNumber = $(".additionally .phone a").text();
    let activeCity = $(".additionally .city span").attr("data-city");
    let choiceCity = $(".open-window .cities div");
    let cityWindow = $(".open-window .window-city");
    let authWindow = $(".open-window .window-registration");


    $(window).resize(function(){
        changeWindow();
    });

  //  let i = 1;

    if(document.documentElement.clientWidth >= 1200){
        $(".single").css("flex-flow", "nowrap");
    }else{
        $(".single").css("flex-flow", "wrap");
    }

    window.addEventListener(`resize`, event => {
        if(document.documentElement.clientWidth >= 1200){
            $(".single").css("flex-flow", "nowrap");
        }else {
            $(".single").css("flex-flow", "wrap");
        }
        changeWindow();
    }, false);

    function changeWindow() {
        if(document.documentElement.clientWidth < 1200){
            $(".additionally .center").css({"display" : "flex", "justify-content" : "space-evenly"});
            $(".single").css("flex-flow", "wrap");
        }else{
            $(".single").css("flex-flow", "nowrap");
            $(".additionally .center").css({"display" : "flex", "justify-content" : "flex-start"});
        }
    }

    $(".open-window .close").click(function () {
        $(".open-window").hide();
        $("html, body").css("overflow-y", "");
        cityWindow.hide();
    });

    choiceCity.each(function () {
       let city = $(this).attr("data-city");
       if(activeCity === city){
            $(this).addClass("active");
       }
        addCity();
    });

    choiceCity.click(function () {
        authWindow.hide();
        let city = $(this).attr("data-city");
        activeCity = city;
        removeCity();
        if(!$(this).hasClass("active")){
            $(this).addClass("active");
            addCity();
            $(".additionally .city span").text($(".open-window .cities .active").text());
        }
    });

    $(".open-window .one-location").click(function () {
        $(".additionally .phone a").text($(this).find("span").text());
        $(".open-window").fadeOut(200);
        cityWindow.hide();
        authWindow.hide();
        $("html, body").css("overflow-y", "");
    });

    function addCity() {
        $(".open-window .locations .location div").each(function () {
            let inCity = $(this).attr("data-city");
            if(inCity === activeCity){
                $(this).show();
            }else {
                $(this).hide();
            }
        });
    }

    function removeCity() {
        choiceCity.each(function () {
            $(this).removeClass("active");
        });
    }

    $(".auth").click(function () {
        $(".open-window").css("display" , "flex");
        $("html, body").css("overflow-y", "hidden");
        authWindow.show();
        $(".auth-block").hide();
        $(".forgot-block").hide();
        $(".window-pay").hide();
        $(".window-calendar").hide();
    });

    $(".window-registration .auth").click(function () {
        $(".window-registration .login-block").hide();
        $(".window-registration .auth-block").show();
        $(".window-registration .forgot-block").hide();
    });

    $(".window-registration .login").click(function () {
        $(".window-registration .login-block").show();
        $(".window-registration .auth-block").hide();
        $(".window-registration .forgot-block").hide();
    });

    $(".window-registration .forgot").click(function () {
        $(".window-registration .login-block").hide();
        $(".window-registration .auth-block").hide();
        $(".window-registration .forgot-block").show();
    });

    
    $(".text .item").click(function () {
        if(!$(this).hasClass("active-item")){
            $($(this)).addClass("active-item");
        }else{
            $($(this)).removeClass("active-item");
        }
    });

    $(".text .pay .item").click(function () {
            $(".open-window").css("display", "flex");
            $("html, body").css("overflow-y", "hidden");
            $(".window-pay").show();
            $(".window-calendar").hide();
    });

    $(".text .doing .item").click(function () {
            $(".open-window").css("display" , "flex");
            $("html, body").css("overflow-y", "hidden");
            $(".window-calendar").show();
            $(".window-pay").hide();

    });



    function Calendar2(id, year, month) {
        var Dlast = new Date(year,month+1,0).getDate(),
            D = new Date(year,month,Dlast),
            DNlast = new Date(D.getFullYear(),D.getMonth(),Dlast).getDay(),
            DNfirst = new Date(D.getFullYear(),D.getMonth(),1).getDay(),
            calendar = '<tr>',
            month=["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"];
        if (DNfirst != 0) {
            for(var  i = 1; i < DNfirst; i++) calendar += '<td>';
        }else{
            for(var  i = 0; i < 6; i++) calendar += '<td>';
        }
        for(var  i = 1; i <= Dlast; i++) {
            if (i == new Date().getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()) {
                calendar += '<td class="today">' + i;
            }else{
                calendar += '<td>' + i;
            }
            if (new Date(D.getFullYear(),D.getMonth(),i).getDay() == 0) {
                calendar += '<tr>';
            }
        }
        for(var  i = DNlast; i < 7; i++) calendar += '<td>&nbsp;';
        document.querySelector('#'+id+' tbody').innerHTML = calendar;
        document.querySelector('#'+id+' thead td:nth-child(2)').innerHTML = month[D.getMonth()] +' '+ D.getFullYear();
        document.querySelector('#'+id+' thead td:nth-child(2)').dataset.month = D.getMonth();
        document.querySelector('#'+id+' thead td:nth-child(2)').dataset.year = D.getFullYear();
        if (document.querySelectorAll('#'+id+' tbody tr').length < 6) {  // чтобы при перелистывании месяцев не "подпрыгивала" вся страница, добавляется ряд пустых клеток. Итог: всегда 6 строк для цифр
            document.querySelector('#'+id+' tbody').innerHTML += '<tr><td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;';
        }
    }
    Calendar2("calendar2", new Date().getFullYear(), new Date().getMonth());
// переключатель минус месяц
    document.querySelector('#calendar2 thead tr:nth-child(1) td:nth-child(1)').onclick = function() {
        Calendar2("calendar2", document.querySelector('#calendar2 thead td:nth-child(2)').dataset.year, parseFloat(document.querySelector('#calendar2 thead td:nth-child(2)').dataset.month)-1);
    }
// переключатель плюс месяц
    document.querySelector('#calendar2 thead tr:nth-child(1) td:nth-child(3)').onclick = function() {
        Calendar2("calendar2", document.querySelector('#calendar2 thead td:nth-child(2)').dataset.year, parseFloat(document.querySelector('#calendar2 thead td:nth-child(2)').dataset.month)+1);
    }

    $("#calendar2 td").click(function () {
        let chData = $(this).text();
        $("#calendar2 td").each(function () {
            $(this).removeClass("today");
        });
        $(this).addClass("today");
    });

    /// 23.10
    $(".open-window .window-calendar .calendar").click(function () {
        $(".open-window #calendar2").hide();
        $(".open-window .window-calendar .title").text("Спасибо! Мы получили Вашу заявку");
        $(".open-window .message")
            .text("В ближайшее время с Вами свяжутся волонтеры для подтверждения даты. Тритон ждет с нетерпением!")
            .css({"margin" : "20px 0px", "max-width" : "400px"});
        $(".open-window input[type=button]").attr("value", "Закрыть");
        if($(this).hasClass("btn-close")){
            $(".open-window").hide();
            $(".window-calendar").hide();
            $(".window-pay").hide();
            $("html, body").css("overflow-y", "auto");
            $(".open-window .message")
                .text("")
                .css({"margin" : "", "max-width" : ""});
            $(".open-window #calendar2").show();
            $(".open-window .window-calendar .title").text("Выберите дату");
            $(".open-window input[type=button]").attr("value", "Подтвердить");
            $(this).removeClass("btn-close");
        }else{
            $(this).addClass("btn-close");
        }

    });

    $(".additionally .city").click(function () {
        $(".open-window").css("display" , "flex");
        $("html, body").css("overflow-y", "hidden");
        cityWindow.show();
        authWindow.hide();
        $(".window-pay").hide();
        $(".window-calendar").hide();
    });




})(jQuery);
