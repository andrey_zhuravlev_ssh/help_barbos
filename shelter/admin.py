from django.contrib import admin

from shelter.models import Shelter


class ShelterAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name',)

admin.site.register(Shelter, ShelterAdmin)







