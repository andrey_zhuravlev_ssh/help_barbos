# -*- coding: utf-8 -*-
from django.db import models


class Shelter(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название")
    full_name = models.CharField(max_length=1024, verbose_name="Полное название", blank=True, null=True, default="")
    description = models.TextField(verbose_name="Описание", blank=True, null=True)
    subdomain_name = models.CharField(max_length=1024, verbose_name="Название поддомена", blank=True, null=True, default="")
    phone = models.CharField(max_length=1024, verbose_name="Телефон",blank=True,null=True, default="")
    email = models.CharField(max_length=256, verbose_name="Электронная почта",blank=True,null=True, default="")
    city = models.CharField(max_length=1024, verbose_name="Город", blank=True, null=True, default="")
    address = models.CharField(max_length=2048, verbose_name="Адрес", blank=True, null=True, default="")
    is_hidden = models.BooleanField(verbose_name="Скрыть", blank=True, default=False)
    group_vk = models.CharField(max_length=1024, verbose_name="Группа ВК", blank=True, null=True, default="")
    geo = models.CharField(max_length=1024, verbose_name="Код карты",blank=True, null=True, default="")

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Приют"
