# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth import logout
from django.shortcuts import redirect, get_object_or_404

from shelter.models import Shelter
from slider.models import Slider
from menu.models import Menu
from animal.models import Animal
from history.models import History
from text_block_menu.models import TextBlockMenu


def send_mail(order=None):
    EMAIL_ADDRESS = "info@himmetproduct.ru"
    EMAIL_PASS = "cDa1GyHy231"


    subject = u"""Поступила новая заявка"""
    # body = u"""Заказ от %s \nФ.И.О: %s \nТелефон: %s \nАдрес электронной почты: %s\nТип заказа: %s\n%s""" % (date, fio, phone, email, type, text)
    body = "Заявка от пользователя"
    from email.mime.text import MIMEText
    from email.mime.multipart import MIMEMultipart
    address = EMAIL_ADDRESS
    email_to = "a.zhuravlev.npoa@mail.ru"

    msg = MIMEMultipart()

    msg['From'] = address
    msg['To'] = email_to
    msg['Subject'] = subject

    textpart = MIMEText(u"%s" % (body), 'plain', 'UTF-8')
    msg.attach(textpart)


    import smtplib
    s = smtplib.SMTP("smtp.mail.ru")
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.debuglevel = 5
    s.login(EMAIL_ADDRESS, EMAIL_PASS)
    s.sendmail(address, email_to, msg.as_string())
    s.quit()


def get_current_shelter(request):
    host = request.get_host()
    current_shelter = None
    shelters = Shelter.objects.all()
    for item in shelters:
        if item.subdomain_name and item.subdomain_name in host:
            current_shelter = item
            break
    if not current_shelter:
        current_shelter = Shelter.objects.first()
    return current_shelter

def logout_view(request):
    logout(request)
    return redirect('/')

def index(request, template_name="index.html"):
    current_shelter = get_current_shelter(request)
    sliders = Slider.objects.filter(shelter=current_shelter, isHidden=False)
    animals = Animal.objects.filter(is_main=True, is_delete=False, shelter=current_shelter)
    histories = [History.objects.filter(isHidden=False).first(),]
    return render(request, template_name, locals())

def show_menu(request, menu_slug, template_name="info.html"):
    current_shelter = get_current_shelter(request)
    current_menu = get_object_or_404(Menu, slug=menu_slug)
    text_blocks = TextBlockMenu.objects.filter(position=current_menu, isHidden=False, shelter=current_shelter)
    template_name = current_menu.type_menu.template
    return render(request, template_name, locals())



def show_animal(request, animal_slug, template_name="animal.html"):
    if request.POST:
        send_mail()
    current_shelter = get_current_shelter(request)
    current_animal = get_object_or_404(Animal, slug=animal_slug, shelter=current_shelter)
    animals = Animal.objects.filter(is_main=True, is_delete=False, shelter=current_shelter)[:3]
    return render(request, template_name, locals())

def histories_view(request, template_name="histories.html"):
    current_shelter = get_current_shelter(request)
    histories = History.objects.filter(shelter=current_shelter, isHidden=False)
    return render(request, template_name, locals())

def rating_view(request, template_name="rating.html"):
    current_shelter = get_current_shelter(request)
    histories = History.objects.filter(shelter=current_shelter, isHidden=False)
    return render(request, template_name, locals())

def get_friend_view(request, template_name="get_friend.html"):
    current_shelter = get_current_shelter(request)
    animals = Animal.objects.filter(is_delete=False, shelter=current_shelter)
    return render(request, template_name, locals())

def global_views(request):
    return {
        'shelters': Shelter.objects.filter(is_hidden=False),
        'current_shelter': get_current_shelter(request),
        'menu_list': Menu.objects.filter(parent__isnull=True, isHidden=False),
    }

