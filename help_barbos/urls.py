"""help_barbos URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.views.static import serve
from django.conf.urls import include, url, re_path
from help_barbos.views import index, logout_view, show_menu, show_animal, histories_view, rating_view, get_friend_view
from help_barbos import settings

urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'^$', index),
    re_path(r'', include('social_django.urls')),
    url(r'^logout/$', logout_view, name='logout'),
    url(r'^histories/$', histories_view),
    url(r'^rating/$', rating_view),
    url(r'^get-friend/$', get_friend_view),
    re_path(r'^animal/(?P<animal_slug>[-\w/]+)/$', show_animal, {}, 'animal'),
    re_path(r'^(?P<menu_slug>[-\w/]+)/$', show_menu, {}, 'menu'),
    re_path(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
]
