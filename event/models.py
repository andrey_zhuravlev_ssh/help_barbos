# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from shelter.models import Shelter
from animal.models import Animal
from action.models import Action


class Event(models.Model):
    user = models.ForeignKey(User, verbose_name="Пользователь", on_delete=models.CASCADE)
    animal = models.ForeignKey(Animal, verbose_name="Животное", on_delete=models.CASCADE)
    shelter = models.ForeignKey(Shelter, verbose_name="Приют", on_delete=models.CASCADE)
    action = models.ForeignKey(Action, verbose_name="Действие", on_delete=models.CASCADE)
    date = models.DateTimeField(verbose_name="Дата"),
    points = models.FloatField(verbose_name="Баллы", blank=True, null=True)
    comment = models.TextField(verbose_name="Комментарии", blank=True, null=True)
    is_happened = models.BooleanField(verbose_name="Событие произошло")

    def __str__(self):
        return self.user.get_full_name() + " " + self.animal.name + " " + self.action.name

    class Meta:
        ordering = ["id"]
        verbose_name_plural = u"События"
