from django.contrib import admin

from event.models import Event


class EventAdmin(admin.ModelAdmin):
    list_display = ('user', )
    # search_fields = ('name',)

admin.site.register(Event, EventAdmin)







