from django.contrib import admin

from animal.models import TypeAnimal, Animal


class TypeAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name',)


class AnimalAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug',)
    search_fields = ('name',)


admin.site.register(TypeAnimal,TypeAdmin)
admin.site.register(Animal, AnimalAdmin)







