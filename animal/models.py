# -*- coding: utf-8 -*-
from django.db import models
from pytils.translit import slugify
from django.db.models.signals import pre_save
from tinymce import models as tinymce_models

from shelter.models import Shelter


class TypeAnimal(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название")
    slug = models.SlugField(max_length=1024, verbose_name="Название латинское", blank=True, null=True)
    description = models.TextField(verbose_name="Описание", blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Тип животного"


class Animal(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название")
    slug = models.SlugField(max_length=1024, verbose_name="Название латинское", blank=True, null=True, default='')
    shelter = models.ForeignKey(Shelter, verbose_name="Приют", on_delete=models.CASCADE)
    type = models.ForeignKey(TypeAnimal, verbose_name="Тип", on_delete=models.CASCADE)
    image = models.ImageField(upload_to='uploads/images', verbose_name="Картинка", blank=True, null=True)
    image_1 = models.ImageField(upload_to='uploads/images', verbose_name="Картинка 1", blank=True, null=True, default='')
    image_2 = models.ImageField(upload_to='uploads/images', verbose_name="Картинка 2", blank=True, null=True, default='')
    image_3 = models.ImageField(upload_to='uploads/images', verbose_name="Картинка 3", blank=True, null=True, default='')
    image_4 = models.ImageField(upload_to='uploads/images', verbose_name="Картинка 4", blank=True, null=True, default='')
    image_5 = models.ImageField(upload_to='uploads/images', verbose_name="Картинка 5", blank=True, null=True, default='')
    sex = models.CharField(max_length=256, verbose_name="Пол", blank=True, null=True, default='')
    age = models.CharField(max_length=256, verbose_name="Возраст", blank=True, null=True, default='')
    text = models.TextField(verbose_name="Текст", blank=True, null=True, default='')
    description = tinymce_models.HTMLField(verbose_name="Описание", blank=True, null=True, default='')
    is_delete = models.BooleanField(verbose_name="Удалено из списка", blank=True, default=False)
    is_main = models.BooleanField(verbose_name="Отображение на главной страницы", blank=True, default=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Животное"

    @models.permalink
    def get_absolute_url(self):
        return 'animal', (), {'animal_slug': self.slug}


def save_filefield(sender, **kwargs):
    item = kwargs.get('instance')
    item.slug = slugify(item.name)


pre_save.connect(save_filefield, Animal)
