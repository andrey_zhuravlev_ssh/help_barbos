# -*- coding: utf-8 -*-
from django.contrib import admin

from text_block_menu.models import TextBlockMenu


class TextBlockMenuAdmin(admin.ModelAdmin):
    list_display = ('name','position')
    list_filter = ('position',)


admin.site.register(TextBlockMenu,TextBlockMenuAdmin)
