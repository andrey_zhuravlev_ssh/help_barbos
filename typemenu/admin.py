# -*- coding: utf-8 -*-
from django.contrib import admin

from typemenu.models import TypeMenu

admin.site.register(TypeMenu)