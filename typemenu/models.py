# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _


class TypeMenu(models.Model):
    name = models.CharField(max_length=256, verbose_name=_(u"Название типа"))
    template = models.CharField(max_length=1024, verbose_name=_(u"Название файла шаблона"))

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["id"]
        verbose_name_plural = _(u"Тип меню")
