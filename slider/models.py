# -*- coding: utf-8 -*-
from django.db import models

from shelter.models import Shelter


class Slider(models.Model):
    order_number = models.IntegerField(verbose_name="Порядковый номер", blank=True, null=True)
    shelter = models.ForeignKey(Shelter, verbose_name="Приют", on_delete=models.CASCADE, default=1)
    name = models.CharField(max_length=1024, verbose_name="Название")
    url = models.CharField(max_length=1024, verbose_name="Ссылка", blank=True, null=True)
    text = models.TextField(max_length=1024, verbose_name="Описание")
    text_button = models.TextField(max_length=1024, verbose_name="Текст (на странице)")
    image = models.ImageField(upload_to='uploads/images', verbose_name="Картинка", blank=True, null=True)
    isHidden = models.BooleanField(verbose_name="Скрыть", blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["order_number"]
        verbose_name_plural = u"Слайдер"
