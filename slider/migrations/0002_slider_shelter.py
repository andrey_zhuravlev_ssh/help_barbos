# Generated by Django 2.0.5 on 2019-10-22 15:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shelter', '0003_shelter_full_name'),
        ('slider', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='slider',
            name='shelter',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='shelter.Shelter', verbose_name='Приют'),
        ),
    ]
