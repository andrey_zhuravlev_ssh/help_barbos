# -*- coding: utf-8 -*-
from django.contrib import admin

from menu.models import Menu


class MenuAdmin(admin.ModelAdmin):
    list_display = ('name', 'parent')
    search_fields = ('name',)

admin.site.register(Menu, MenuAdmin)
