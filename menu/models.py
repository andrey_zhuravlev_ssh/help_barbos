# -*- coding: utf-8 -*-
from django.db import models
from typemenu.models import TypeMenu
from django.db.models.signals import pre_save
from pytils.translit import slugify


class Menu(models.Model):
    parent = models.ForeignKey('self', verbose_name="Родительский пункт", null=True, blank=True, on_delete=models.CASCADE)
    order_number = models.FloatField(verbose_name="Порядковый номер", blank=True, null=True)
    name = models.CharField(max_length=1024, verbose_name="Название пункта")
    slug = models.SlugField(max_length=1024, verbose_name="Название латинское", blank=True)
    type_menu = models.ForeignKey(TypeMenu, verbose_name="Тип меню", on_delete=models.CASCADE)
    title_main = models.CharField(max_length=1024, verbose_name="Заголовок страницы", blank=True,null=True)
    image = models.ImageField(upload_to='uploads/images', verbose_name="Картинка", blank=True, null=True, editable=False)
    isHidden = models.BooleanField(verbose_name="Скрыть", blank=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["order_number"]
        verbose_name_plural = u"Меню"

    @models.permalink
    def get_absolute_url(self):
        return 'menu', (), {'menu_slug': self.slug}

    def get_child_menu(self):
        return Menu.objects.filter(parent=self, isHidden=False)


def save_filefield(sender, **kwargs):
    item = kwargs.get('instance')
    if not item.slug:
        item.slug = slugify(item.name)


pre_save.connect(save_filefield, Menu)