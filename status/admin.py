from django.contrib import admin

from status.models import Status


class StatusAdmin(admin.ModelAdmin):
    list_display = ('name', 'points', 'description')
    search_fields = ('name',)

admin.site.register(Status,StatusAdmin)







