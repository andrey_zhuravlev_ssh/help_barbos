# -*- coding: utf-8 -*-
from django.db import models
from django.db.models.signals import pre_save

from django.contrib.auth.models import User
from action.models import save_filefield


class Status(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название")
    slug = models.SlugField(max_length=1024, verbose_name="Название латинское", blank=True, null=True)
    points = models.FloatField(verbose_name="Баллы", blank=True, null=True)
    description = models.TextField(verbose_name="Описание", blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Статус пользователя"

pre_save.connect(save_filefield, Status)