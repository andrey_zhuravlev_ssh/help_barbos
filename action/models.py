# -*- coding: utf-8 -*-
from django.db import models
from pytils.translit import slugify

from django.db.models.signals import pre_save


class Action(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название")
    slug = models.SlugField(max_length=1024, verbose_name="Латинское название", blank=True, null=True)
    points = models.FloatField(verbose_name="Баллы", blank=True, null=True)
    description = models.TextField(verbose_name="Описание", blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Действия"


def save_filefield(sender, **kwargs):
    item = kwargs.get('instance')
    item.slug = slugify(item.name)


pre_save.connect(save_filefield, Action)
