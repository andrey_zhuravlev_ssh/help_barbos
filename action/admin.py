from django.contrib import admin

from action.models import Action


class ActionAdmin(admin.ModelAdmin):
    list_display = ('name', 'points', 'description')
    search_fields = ('name',)

admin.site.register(Action,ActionAdmin)







